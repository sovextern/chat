//
//  CBMessageCellView.h
//  CBChat
//
//  Created by Наиль  on 20.07.17.
//  Copyright © 2017 Наиль . All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol CBMessage;

@class CBLayout;

@interface CBMessageCellView : UICollectionViewCell

@property (nonatomic, strong) UIColor *textColor;

@property (nonatomic, strong) UIColor *timeColor;

@property (nonatomic, strong) UIFont *font;

- (void) configWithMessage:(id<CBMessage>)message;

- (void) setupLayout:(CBLayout *)layout;

@end
