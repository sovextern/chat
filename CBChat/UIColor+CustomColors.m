//
//  UIColor+CoreCustomColors.m
//  
//  Created by ios on 03.05.16.
//  Copyright © 2016 All rights reserved.
//

#import "UIColor+CustomColors.h"

@implementation UIColor (CoreCustomColors)

+ (UIColor*)colorWith8BitRed:(NSInteger)red green:(NSInteger)green blue:(NSInteger)blue alpha:(CGFloat)alpha
{
    return [UIColor colorWithRed:((CGFloat)red/255.0f) green:((CGFloat)green/255.0f) blue:((CGFloat)blue/255.0f) alpha:alpha];
}

+ (UIColor*)colorWithHex:(NSString*)hex alpha:(CGFloat)alpha
{
    const char* hexStr = [hex UTF8String];
    if( hexStr == NULL || *hexStr != '#' )
    {
        return nil;
    }

    int r = 0, g = 0, b = 0;
    if( sscanf(hexStr, "#%02x%02x%02x", &r, &g, &b) != 3 )
    {
        return nil;
    }
    return [UIColor colorWith8BitRed:r green:g blue:b alpha:alpha];
}

+ (UIColor *) generateFromHexRandomColor
{
    NSInteger baseInt = arc4random() % 16777216;
    NSString *hex = [NSString stringWithFormat:@"#%06lX", (long)baseInt];
    return [UIColor colorWithHex:hex alpha:1.0];
}

@end
