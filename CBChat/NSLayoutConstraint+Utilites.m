//
//  NSLayoutConstraint+SoulUtilites.m
//
//  Created by ios on 03.05.16.
//  Copyright © 2016 All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NSLayoutConstraint+Utilites.h"


@implementation NSLayoutConstraint(SoulUtilites)

+ (NSLayoutConstraint *) leadingFrom:(id)from to:(id)to constant:(CGFloat)constant
{
    return [NSLayoutConstraint constraintWithItem:from attribute:NSLayoutAttributeLeading relatedBy:NSLayoutRelationEqual toItem:to attribute:NSLayoutAttributeLeading multiplier:1.0 constant:constant];
}

+ (NSLayoutConstraint *) trailingFrom:(id)from to:(id)to constant:(CGFloat)constant
{
    return [NSLayoutConstraint constraintWithItem:from attribute:NSLayoutAttributeTrailing relatedBy:NSLayoutRelationEqual toItem:to attribute:NSLayoutAttributeTrailing multiplier:1.0 constant:constant];
}

+ (NSLayoutConstraint *) topFrom:(id)from to:(id)to constant:(CGFloat)constant
{
    return [NSLayoutConstraint constraintWithItem:from attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:to attribute:NSLayoutAttributeTop multiplier:1.0 constant:constant];
}

+ (NSLayoutConstraint *) fromAttribute:(NSLayoutAttribute)attr from:(id)from topTo:(id)to constant:(CGFloat)constant
{
    return [NSLayoutConstraint constraintWithItem:to attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:from attribute:attr multiplier:1.0 constant:constant];
}

+ (NSLayoutConstraint *) bottomFrom:(id)from to:(id)to constant:(CGFloat)constant
{
    return [NSLayoutConstraint constraintWithItem:from attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:to attribute:NSLayoutAttributeBottom multiplier:1.0 constant:constant];
}

+ (NSLayoutConstraint *) heightFor:(id)object constant:(CGFloat)constant
{
    return [NSLayoutConstraint constraintWithItem:object attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0 constant:constant];
}

+ (NSLayoutConstraint *) widthFor:(id)object constant:(CGFloat)constant
{
    return [NSLayoutConstraint constraintWithItem:object attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0 constant:constant];
}

+ (NSLayoutConstraint *) centerY:(id)from to:(id)to constant:(CGFloat)constant
{
    return [NSLayoutConstraint constraintWithItem:from attribute:NSLayoutAttributeCenterY relatedBy:NSLayoutRelationEqual toItem:to attribute:NSLayoutAttributeCenterY multiplier:1.0 constant:constant];
}

+ (NSLayoutConstraint *) centerX:(id)from to:(id)to constant:(CGFloat)constant
{
    return [NSLayoutConstraint constraintWithItem:from attribute:NSLayoutAttributeCenterX relatedBy:NSLayoutRelationEqual toItem:to attribute:NSLayoutAttributeCenterX multiplier:1.0 constant:constant];
}


+ (NSLayoutConstraint *) equalWidthFrom:(id)from to:(id) to
{
    return [NSLayoutConstraint constraintWithItem:from attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:to attribute:NSLayoutAttributeWidth multiplier:1.0 constant:0.0];
}

+ (NSLayoutConstraint *) fromAttribute:(NSLayoutAttribute)attr from:(id)from toAttribute:(NSLayoutAttribute)toAttr to:(id)to constant:(CGFloat)constant
{
    return [NSLayoutConstraint constraintWithItem:from attribute:attr relatedBy:NSLayoutRelationEqual toItem:to attribute:toAttr multiplier:1.0 constant:constant];
}

+ (NSLayoutConstraint *) acceptRatioFor:(id)object withMultiplier:(CGFloat)multiplier
{
    return [NSLayoutConstraint constraintWithItem:object attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:object attribute:NSLayoutAttributeHeight multiplier:multiplier constant:0.0f];
}

+ (NSArray *) standartBoxConstratinsFrom:(id)from to:(id)to constant:(CGFloat)constant
{
    NSMutableArray *constraints = [[NSMutableArray alloc] init];
    
    [constraints addObject:[NSLayoutConstraint leadingFrom:from to:to constant:constant]];
    [constraints addObject:[NSLayoutConstraint trailingFrom:to to:from constant:constant]];
    [constraints addObject:[NSLayoutConstraint topFrom:to to:from constant:constant]];
    [constraints addObject:[NSLayoutConstraint bottomFrom:from to:to constant:constant]];
    
    return constraints;
}

@end
