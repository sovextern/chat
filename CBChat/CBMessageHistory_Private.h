//
//  CBMessageHistory_Private.h
//  CBChat
//
//  Created by Наиль  on 21.07.17.
//  Copyright © 2017 Наиль . All rights reserved.
//

#import "CBMessageHistory.h"

@interface CBMessageHistory()

- (void) addMessage:(id<CBMessage>)message;

- (void) addMessages:(NSArray<CBMessage> *)messages;

- (NSTimeInterval) sectionDate:(NSUInteger)section;

- (void) showedHistoryAtSection:(NSUInteger)section withIndex:(NSUInteger)index;

- (void) allIndexPathsWithCompletionBlock:(void(^)(NSArray<NSIndexPath *> *))completionBlock;

@end
