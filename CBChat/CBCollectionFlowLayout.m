
//
//  CBCollectionFlowLayout.m
//  CBChat
//
//  Created by Наиль  on 20.07.17.
//  Copyright © 2017 Наиль . All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CBCollectionFlowLayout.h"
#import "CBCollectionView.h"

#import "CBHashKey.h"

#import "UICollectionViewLayoutAttributes+CBHashUtils.h"

@interface CBCollectionFlowLayout ()

@property (nonatomic, assign) CGSize contentSize;

@property (nonatomic, strong) NSMutableArray *insertedIndexPaths;

@property (nonatomic, strong) NSMutableDictionary *cachedAttributes;

@end

@implementation CBCollectionFlowLayout

#pragma mark - init methods

- (instancetype) init
{
    if ( (self = [super init]) == nil)
    {
        return nil;
    }
     return self;
}

#pragma mark - ovveride methods

- (nullable NSArray<__kindof UICollectionViewLayoutAttributes *> *)layoutAttributesForElementsInRect:(CGRect)rect
{
    NSInteger numberOfSection = [self.collectionView numberOfSections];

    CBCollectionView *collectionView = self.collectionView;
    
    if ([collectionView numberOfAllItems] > self.cachedAttributes.count)
    {
        self.cachedAttributes = nil;
    }
    
    if (!self.cachedAttributes)
    {
        self.cachedAttributes = [NSMutableDictionary new];
        UICollectionViewLayoutAttributes *lastAttr = nil;
        for (NSInteger section = 0; section < numberOfSection; section++)
        {
            CGFloat maxY = lastAttr ?  CGRectGetMaxY(lastAttr.frame) : 0;
            NSInteger items = [self.collectionView numberOfItemsInSection:section];
            UICollectionViewLayoutAttributes *attr = [UICollectionViewLayoutAttributes layoutAttributesForSupplementaryViewOfKind:UICollectionElementKindSectionHeader withIndexPath:[NSIndexPath indexPathForRow:0 inSection:section]];
            UICollectionViewLayoutAttributes *updatedAttr = [self updateLayoutAttributeHeader:attr withOffestByY:maxY];
            [self cacheLayoutAttribute:updatedAttr];
            lastAttr = updatedAttr;
            
            for (NSInteger row = 0; row < items; row ++)
            {
                maxY = lastAttr ?  CGRectGetMaxY(lastAttr.frame) : 0;
                UICollectionViewLayoutAttributes *attr = [UICollectionViewLayoutAttributes layoutAttributesForCellWithIndexPath:[NSIndexPath indexPathForRow:row inSection:section]];
                UICollectionViewLayoutAttributes *updatedAttr = [self updateLayoutAttributeCell:attr withOffsetByY:maxY];
                [self cacheLayoutAttribute:updatedAttr];
                lastAttr = updatedAttr;
            }
        }
        
        [self updateContentSize];
        
        if (CGSizeEqualToSize(CGSizeZero, self.contentSize))
        {
            return self.cachedAttributes.allValues;
        }
    }
    
    NSArray *filteredAttributes = [[self.cachedAttributes allValues] filteredArrayUsingPredicate:[self predicateCollectionLayoutAttributesWithRect:rect]];
    
    return filteredAttributes;
}

- (CGSize) collectionViewContentSize
{
    return self.contentSize;
}

#pragma mark - utils methods

- (UICollectionViewLayoutAttributes *) updateLayoutAttributeHeader:(UICollectionViewLayoutAttributes *)attr withOffestByY:(CGFloat)offsetY
{
    UIEdgeInsets insets = [self sectionInsetForSection:attr.indexPath.section];
    CGSize size = [self headerReferenceSizeForSection:attr.indexPath.section];
    CGRect frame = attr.frame;
    frame.origin.y = offsetY;
    frame.size = size;
    frame = UIEdgeInsetsInsetRect(frame, insets);
    attr.frame = frame;
    return attr;
}
                           
- (UICollectionViewLayoutAttributes *) updateLayoutAttributeCell:(UICollectionViewLayoutAttributes *)attr withOffsetByY:(CGFloat)offsetY
{
    CGPoint offset = [self cellOffetstForIndexPath:attr.indexPath];
    CGRect frame = attr.frame;
    frame.size = [self cellSizeForIndexPath:attr.indexPath];
    frame.origin.x = offset.x;
    frame.origin.y = offsetY + offset.y;
    attr.frame = frame;
    
    return attr;
}

- (CGPoint) cellOffetstForIndexPath:(NSIndexPath *)indexPath
{
   CGPoint offset = CGPointZero;
   if ([self.delegate respondsToSelector:@selector(collectionView:cellOffsetAtIndexPath:)])
   {
       offset = [self.delegate collectionView:self.collectionView cellOffsetAtIndexPath:indexPath];
   }
    return offset;
}
                           
- (CGSize) headerReferenceSizeForSection:(NSInteger) section
{
    CGSize size = self.headerReferenceSize;
    
    if ([self.delegate respondsToSelector:@selector(collectionView:layout:referenceSizeForHeaderInSection:)])
    {
        size = [self.delegate collectionView:self.collectionView layout:self referenceSizeForHeaderInSection:section];
    }

    return size;
}

- (UIEdgeInsets) sectionInsetForSection:(NSInteger) section
{
    UIEdgeInsets insets = self.sectionInset;
    
    if ([self.delegate respondsToSelector:@selector(collectionView:layout:insetForSectionAtIndex:)])
    {
        insets = [self.delegate collectionView:self.collectionView layout:self insetForSectionAtIndex:section];
    }
    
    return insets;
}

- (CGSize) cellSizeForIndexPath:(NSIndexPath *)indexPath
{
    CGSize size = CGSizeZero;
    
    if ( [self.delegate respondsToSelector:@selector(collectionView:layout:sizeForItemAtIndexPath:)] )
    {
        size = [self.delegate collectionView:self.collectionView layout:self sizeForItemAtIndexPath:indexPath];
    }
    
    return size;
}

- (NSPredicate *) predicateCollectionLayoutAttributesWithRect:(CGRect)rect
{
    return [NSPredicate predicateWithBlock:^BOOL(id  _Nullable evaluatedObject, NSDictionary<NSString *,id> * _Nullable bindings) {
        UICollectionViewLayoutAttributes *attribute = evaluatedObject;
        BOOL res =  CGRectIntersectsRect(rect, attribute.frame);
        
        return res;
    }];
}

- (void) cacheLayoutAttribute:(UICollectionViewLayoutAttributes *)attr
{
    CBHashKey *hashKey = [CBHashKey keyWithHash:[attr hashByKindAndIndexPath]];
    [self.cachedAttributes setObject:attr forKey:hashKey];
}

- (void) updateContentSize
{
    NSSortDescriptor *sortDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"indexPath" ascending:NO];
    NSArray *sorted = [[self.cachedAttributes allValues] sortedArrayUsingDescriptors:@[sortDescriptor]];
    UICollectionViewLayoutAttributes *attr = sorted.firstObject;
    CGFloat maxY = CGRectGetMaxY(attr.frame);
    
    CGSize size = self.collectionView.frame.size;
    if (maxY > self.contentSize.height)
    {
        self.contentSize = CGSizeMake(size.width, maxY+10);
    }
    
    self.collectionView.contentOffset = CGPointMake(0, maxY+5);
}



@end
