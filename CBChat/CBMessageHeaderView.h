//
//  CBMessageHeaderView.h
//  CBChat
//
//  Created by Наиль  on 21.07.17.
//  Copyright © 2017 Наиль . All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CBMessageHeaderView : UICollectionReusableView

@property (nonatomic, strong) UILabel *lbText;

@end
