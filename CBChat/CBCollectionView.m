//
//  CBCollectionView.m
//  CBChat
//
//  Created by Наиль  on 24.07.17.
//  Copyright © 2017 Наиль . All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CBCollectionView.h"

@implementation CBCollectionView

#pragma mark - Publick API methods

- (NSArray *) filterInsertedIndexPaths:(NSArray *)indexPaths
{
    NSMutableArray *mutable = [NSMutableArray new];
    for (NSIndexPath *indexPath in indexPaths)
    {
        NSUInteger section = indexPath.section;
        NSUInteger row = indexPath.row;
        
        NSUInteger items = [self numberOfItemsInSection:section];
        
        if (items <= row)
        {
           [mutable addObject:indexPath];
        }
    }
    
    return [mutable copy];
}

- (NSUInteger) numberOfAllItems
{
    NSUInteger numberOfSections = [self numberOfSections];
    NSUInteger sum = 0;
    
    for (NSUInteger section = 0; section < numberOfSections; section++ )
    {
        sum +=  [self numberOfItemsInSection:section];
    }
    
    return sum + numberOfSections;
}

- (CGRect) cellFrameAtIndexPath:(NSIndexPath *)indexPath
{
    UICollectionViewCell *cell = [self cellForItemAtIndexPath:indexPath];
    return cell.frame;
}

- (void) scrollToBottom
{
    CGSize contentSize = self.contentSize;
    CGRect rect = self.frame;
    
    if (CGRectGetHeight(rect) > contentSize.height)
    {
        return;
    }
    CGFloat offsetY = contentSize.height - CGRectGetHeight(rect);
    [self setContentOffset:CGPointMake(0, offsetY) animated:YES];
}

@end
