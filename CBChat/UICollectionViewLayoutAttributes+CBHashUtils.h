//
//  UICollectionViewAttributes+CBHashUtils.h
//  CBChat
//
//  Created by Наиль  on 28.07.17.
//  Copyright © 2017 Наиль . All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UICollectionViewLayoutAttributes(CBHashUtils)

- (NSUInteger) hashByKindAndIndexPath;

@end
