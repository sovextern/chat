//
//  CBMessageCellView.m
//  CBChat
//
//  Created by Наиль  on 20.07.17.
//  Copyright © 2017 Наиль . All rights reserved.
//

#import <Foundation/Foundation.h>

#import "CBMessageCellView.h"
#import "CBMessage.h"
#import "CBLayout.h"

#import "NSDate+CBUtils.h"
#import "NSLayoutConstraint+Utilites.h"

@interface CBMessageCellView()

@property (nonatomic, strong) UILabel *lbText;
@property (nonatomic, strong) UILabel *lbTime;

@end

@implementation CBMessageCellView

#pragma mark - init methods

- (instancetype) init
{
    if ( (self = [super init]) == nil)
    {
        return nil;
    }
    [self commonInit];
    return self;
}

- (instancetype) initWithFrame:(CGRect)frame
{
    if ( (self = [super initWithFrame:frame]) == nil)
    {
        return nil;
    }
    [self commonInit];
    return self;
}

- (void) commonInit
{
    UILabel *lbText = [[UILabel alloc] init];
    lbText.translatesAutoresizingMaskIntoConstraints = NO;
    lbText.font = [UIFont systemFontOfSize:14];
    lbText.lineBreakMode = NSLineBreakByWordWrapping;
    lbText.adjustsFontSizeToFitWidth = YES;
    lbText.numberOfLines = 0;
    
    [self.contentView addSubview:lbText];
    
    [self addConstraint:[NSLayoutConstraint leadingFrom:lbText to:self constant:10]];
    [self addConstraint:[NSLayoutConstraint topFrom:lbText to:self constant:15]];
    [self addConstraint:[NSLayoutConstraint trailingFrom:self to:lbText constant:14]];
    
    self.lbText = lbText;
    
    UILabel *lbTime = [[UILabel alloc] init];
    lbTime.translatesAutoresizingMaskIntoConstraints = NO;
    lbTime.font = [UIFont systemFontOfSize:13];
    
    [self.contentView addSubview:lbTime];
    
    [self addConstraint:[NSLayoutConstraint trailingFrom:self to:lbTime constant:15]];
    [self addConstraint:[NSLayoutConstraint bottomFrom:self to:lbTime constant:10]];
    [self addConstraint:[NSLayoutConstraint fromAttribute:NSLayoutAttributeBottom from:lbText topTo:lbTime constant:10.f]];
    [self addConstraint:[NSLayoutConstraint heightFor:lbTime constant:11.0f]];
    
    self.lbTime = lbTime;
}

- (void) didMoveToWindow
{
    [super didMoveToWindow];
    
    self.lbText.font = self.font;
    self.layer.cornerRadius = 10.0f;
}

#pragma mark - Publick API methods

- (void) configWithMessage:(id<CBMessage>)message
{
    self.lbText.text = message.text;
    self.lbTime.text = [NSDate stringDateFromTimeInterval:message.time inFormat:@"HH:mm" timeZone:nil];
}

- (void) setupLayout:(CBLayout *)layout
{
    self.backgroundColor = layout.backgroundColor;
    self.lbText.textColor = layout.textColor;
    self.lbTime.textColor = layout.timeColor;
    self.lbText.textAlignment = layout.textAligment;
    self.lbTime.textAlignment = layout.timeAligment;
}

@end
