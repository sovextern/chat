//
//  CBMessageStorage.m
//  CBChat
//
//  Created by Наиль  on 20.07.17.
//  Copyright © 2017 Наиль . All rights reserved.
//

#import <Foundation/Foundation.h>

#import "CBLayout.h"

#import "CBMessage.h"
#import "CBMessageStorage_Private.h"
#import "CBMessageHistory_Private.h"

@interface CBMessageStorage()

@property (nonatomic, strong) CBMessageHistory *history;

@end

@implementation CBMessageStorage

- (instancetype) init
{
    if ( (self = [super init]) == nil)
    {
        return nil;
    }
    
    self.history = [[CBMessageHistory alloc] init];
    [self.history addObserver:self forKeyPath:@"filteredMessages" options:NSKeyValueObservingOptionNew context:nil];
    
    return self;
}

- (void) dealloc
{
    [self.history removeObserver:self forKeyPath:@"filteredMessages"];
    NSLog(@"%@ deallocated",NSStringFromClass(self.class));
}

#pragma mark - observer methods

- (void) observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary<NSKeyValueChangeKey,id> *)change context:(void *)context
{
    if ([keyPath isEqualToString:@"filteredMessages"])
    {
        if ([self.pDelegate respondsToSelector:@selector(messageStorageCompleteBuildPartOfMessages:)])
        {
            [self.pDelegate messageStorageCompleteBuildPartOfMessages:self];
        }
        
        return;
    }
}

#pragma mark - Publick API methods

- (void) addMessage:(id<CBMessage>)message
{
    [self.history addMessage:message];
}

- (void) addMessages:(NSArray<CBMessage> *)messages
{
    [self.history addMessages:messages];
}

- (id<CBMessage>) messageInSection:(NSInteger)section atIndex:(NSInteger)index
{
    return [self.history messageAtIndex:index inSection:section];
}



@end
