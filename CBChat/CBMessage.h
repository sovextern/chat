//
//  CBMessage.h
//  CBChat
//
//  Created by Наиль  on 20.07.17.
//  Copyright © 2017 Наиль . All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol CBMessage <NSObject>

@required

@property (nonatomic, strong) NSString *text;
@property (nonatomic, assign) NSTimeInterval time;
@property (nonatomic, assign, getter=isStaff) BOOL staff;

@end

@interface CBMessage : NSObject<CBMessage>




@end
