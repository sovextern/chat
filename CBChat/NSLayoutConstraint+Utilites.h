//
//  NSConstraints+SoulUtilites.h
//
//  Created by ios on 03.05.16.
//  Copyright © 2016. All rights reserved.
//

#import <UIKit/UIKit.h>

/*
 * Utility method category. Provides standart constraints
 */

@interface  NSLayoutConstraint(SoulUtilites)

/* Create leading constraint with defaults multiplier: 1.0
 * @param from - first object
 * @param to - second object
 * @param constant - distance between from & to
 */
+(NSLayoutConstraint *) leadingFrom:(id)from to:(id)to constant:(CGFloat)constant;

/* Create trailing constraint with defaults multiplier: 1.0
 * @param from - first object
 * @param to - second object
 * @param constant - distance between from & to
 */
+(NSLayoutConstraint *) trailingFrom:(id)from to:(id)to constant:(CGFloat)constant;

/* Create top constraint with defaults multiplier: 1.0
 * @param form - first object
 * @param to - second object
 * @param constant - distance between from & to
 */
+(NSLayoutConstraint *) topFrom:(id)from to:(id)to constant:(CGFloat)constant;

/* Create bottom constraint with defaults multiplier: 1.0
 * @param from - first object
 * @param to - second object
 * @param constant - distance between from & to
 */
+(NSLayoutConstraint *) bottomFrom:(id)from to:(id)to constant:(CGFloat)constant;

/* Create center Y constraint with defaults multiplier: 1.0
 * @param from - first object
 * @param to - second object
 * @param constant - distance between from & to
 */
+(NSLayoutConstraint *) centerY:(id)from to:(id)to constant:(CGFloat)constant;

/* Create center Y constraint with defaults multiplier: 1.0
 * @param from - first object
 * @param to - second object
 * @param constant - distance between from & to
 */
+(NSLayoutConstraint *) centerX:(id)from to:(id)to constant:(CGFloat)constant;

/* Create height constraint for object
 * @param object - for this object creat height
 * @param constant - height value
 */
+(NSLayoutConstraint *) heightFor:(id)object constant:(CGFloat)constant;

/* Create width constraint for object
 * @param object - for this object creat height
 * @param constant - height value
 */
+ (NSLayoutConstraint *) widthFor:(id)object constant:(CGFloat)constant;

/* Create width constraint with equal relaion between from&to
 * @param from - first object
 * @param to - second object
 */
+(NSLayoutConstraint *) equalWidthFrom:(id)from to:(id) to;

+ (NSLayoutConstraint *) acceptRatioFor:(id)object withMultiplier:(CGFloat)multiplier;


/** Create constraint from custom attribute to top between from&to
 *
 * @param from - first object
 * @param attr - custom attribute
 * @param to - second object
 * @param constant - distance between from & to
 */
+ (NSLayoutConstraint *) fromAttribute:(NSLayoutAttribute)attr from:(id)from topTo:(id)to constant:(CGFloat)constant;

/** Create constraint from custom attribute to custom attribute between from&to
 *
 * @param from - first object
 * @param attr - from custom attribute
 * @param to - second object
 * @param toAttr - to custom attibute
 * @param constant - distance between from & to
 */
+ (NSLayoutConstraint *) fromAttribute:(NSLayoutAttribute)attr from:(id)from toAttribute:(NSLayoutAttribute)toAttr to:(id)to constant:(CGFloat)constant;

/** Create and return standart box constraint
 *
 * @discussion Create and return standart box constraint. This is trailing/leading and top/bottom constraint with custom constant value 
 *
 * @param from - first object
 * @param to - second object
 * @param constnat - distance between from & to
 */
+ (NSArray *) standartBoxConstratinsFrom:(id)from to:(id)to constant:(CGFloat) constant;



@end
