//
//  CBHistoryArray.m
//  CBChat
//
//  Created by Наиль  on 24.07.17.
//  Copyright © 2017 Наиль . All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CBHistorySection.h"

#import "CBHistoryRow.h"
#import "CBMessage.h"

@interface CBHistorySection()

@property (nonatomic, strong) NSArray<CBHistoryRow *> *historyArray;

@end

@implementation CBHistorySection

- (instancetype) initWithHistoryArray:(NSArray<CBHistoryRow *> *)history withIdentifier:(NSUInteger)identifier
{
    if ( ( self = [super init]) == nil)
    {
        return nil;
    }
    self.historyArray = history;
    self.id = identifier;
    return self;
}

- (instancetype) initWithIdentifier:(NSUInteger)identifier
{
    if ( ( self = [super init]) == nil )
    {
        return nil;
    }
    
    self.historyArray = [NSArray new];
    self.id = identifier;
    return self;
}

#pragma mark - Publick API methods

+ (id) historySectionWithMessages:(NSArray<CBMessage> *)messages withIdentifier:(NSUInteger)identifier
{
    if (!messages)
    {
        return nil;
    }
    
    NSSortDescriptor *timeSortDescription = [NSSortDescriptor sortDescriptorWithKey:@"time" ascending:YES];
    messages = [messages sortedArrayUsingDescriptors:@[timeSortDescription]];
    
    NSArray<CBHistoryRow *> *history = [self historyArrayFromMessages:messages];
    return [[CBHistorySection alloc] initWithHistoryArray:history withIdentifier:identifier];
}

+ (id) historySectionWithIdentifier:(NSUInteger)identifier
{
    return [[CBHistorySection alloc] initWithIdentifier:identifier];
}

- (BOOL) addMessage:(id<CBMessage>)message
{
    CBHistoryRow *history = [self.class historyFromMessage:message];
    
    NSUInteger index = [self.historyArray indexOfObject:history];
    NSMutableArray *m = [self.historyArray mutableCopy];
    
    if (index != NSNotFound)
    {
        return NO;
    }
    
    [m addObject:history];
    
    NSSortDescriptor *timeSortDescription = [NSSortDescriptor sortDescriptorWithKey:@"message.time" ascending:YES];
    [m sortUsingDescriptors:@[timeSortDescription]];
    
    self.historyArray = [m copy];
    return YES;
}

- (void) addMessages:(NSArray<CBMessage> *)messages
{
    for (id<CBMessage> m in messages)
    {
        [self addMessage:m];
    }
}

- (CBHistoryRow *) historyAtIndex:(NSUInteger)index
{
    if (self.historyArray.count <= index)
    {
        return nil;
    }
    
    return [self.historyArray objectAtIndex:index];
}

- (NSUInteger) count
{
    return self.historyArray.count;
}

- (BOOL) isAllShowed
{
    return [self.historyArray filteredArrayUsingPredicate:[self predicateFilterByShowed:NO]].count == 0;
}

- (NSIndexSet *) allMessagesIndexs
{
    return [self.historyArray indexesOfObjectsPassingTest:^BOOL(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        return YES;
    }];
}

#pragma mark - utils methods

- (NSPredicate *) predicateFilterByShowed:(BOOL)showed
{
    return [NSPredicate predicateWithFormat:@"self.showed == %@",@(showed)];
}

+ (NSArray< CBHistoryRow *> *) historyArrayFromMessages:(NSArray<CBMessage> *)messages
{
    NSMutableArray< CBHistoryRow *> *mutable = [NSMutableArray new];
    for (id<CBMessage> m in messages)
    {
         CBHistoryRow *h = [self historyFromMessage:m];
        [mutable addObject:h];
    }
    
    return [mutable copy];
}

+ ( CBHistoryRow *) historyFromMessage:(id<CBMessage>)message
{
     CBHistoryRow *history = [[ CBHistoryRow alloc] init];
    history.message = message;
    history.showed = NO;
    history.id = [self hashForMessage:message];
    
    return history;
}

+ (NSUInteger) hashForMessage:(id<CBMessage>)message
{
    NSUInteger textHash = [message.text length];
    return textHash+message.time;
}

@end
