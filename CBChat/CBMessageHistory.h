//
//  CBMessageStory.h
//  CBChat
//
//  Created by Наиль  on 20.07.17.
//  Copyright © 2017 Наиль . All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@protocol CBMessage;
@interface CBMessageHistory : NSObject

- (NSInteger) numberOfSections;

- (NSInteger) numberOfItemInSection:(NSUInteger)section;

- (id<CBMessage>) messageAtIndex:(NSUInteger)index inSection:(NSInteger)section;

@end
