//
//  CBHistory.h
//  CBChat
//
//  Created by Наиль  on 24.07.17.
//  Copyright © 2017 Наиль . All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol CBMessage;

@interface CBHistoryRow : NSObject

@property (nonatomic, strong) id<CBMessage> message;
@property (nonatomic, assign) NSInteger id;
@property (nonatomic, assign) BOOL showed;

@end
