//
//  UIColor+CoreCustomColors.h
//
//  Created by ios on 03.05.16.
//  Copyright © 2016. All rights reserved.
//

#import <UIKit/UIKit.h>

/**
 * Collection of UIColor instance factory methods.
 */
@interface UIColor (CoreCustomColors)

/**
 *  Returns UIColor object with requested color, defined by RGBA components.
 *
 *  @param red   Red color integet value. MUST be in range 0 - 255
 *  @param green Green color integet value. MUST be in range 0 - 255
 *  @param blue  Blue color integet value. MUST be in range 0 - 255
 *  @param alpha Alpha-channel. MUST be in range 0.0 - 1.0
 *  @return Requested color
 */
+ (UIColor*)colorWith8BitRed:(NSInteger)red green:(NSInteger)green blue:(NSInteger)blue alpha:(CGFloat)alpha;

/**
 *  Returns UIColor object with requested color, defined by hex string.
 *
 *  @param hex   String with color. MUST be in format "#FF00CC"
 *  @param alpha Alpha-channel. MUST be in range 0.0 - 1.0
 *  @return Requested color
 */
+ (UIColor*)colorWithHex:(NSString*)hex alpha:(CGFloat)alpha;


+ (UIColor *) generateFromHexRandomColor;

@end
