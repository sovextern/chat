
//
//  CBHexKey.m
//  CBChat
//
//  Created by Наиль  on 21.07.17.
//  Copyright © 2017 Наиль . All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CBHashKey.h"

@interface CBHashKey()

@property (nonatomic, assign) NSUInteger _hash;

@end

@implementation CBHashKey

+ (id) keyWithHash:(NSUInteger)hash
{
    CBHashKey *key = [[CBHashKey alloc] init];
    key._hash = hash;
    
    return key;
}

- (NSUInteger) hash
{
    return __hash;
}

- (BOOL) isEqual:(id)object
{
    CBHashKey *other = object;
    
    return other._hash == self._hash;
}

#pragma mark - NSCopying

- (id)copyWithZone:(nullable NSZone *)zone
{
    CBHashKey *other = [[CBHashKey alloc] init];
    other._hash = self._hash;
    
    return other;
}


@end
