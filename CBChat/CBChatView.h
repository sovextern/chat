//
//  CBChatView.h
//  CBChat
//
//  Created by Наиль  on 20.07.17.
//  Copyright © 2017 Наиль . All rights reserved.
//

#import <UIKit/UIKit.h>

#import "CBBottomView.h"

@class CBMessageStorage;
@protocol CBMessage;


@interface CBChatView : UIView

@property (nonatomic, strong) UIColor *tempDefaultBackgroundColor UI_APPEARANCE_SELECTOR;

@property (nonatomic, strong) UIFont *tempDefaultFont UI_APPEARANCE_SELECTOR;

@property (nonatomic, weak) CBMessageStorage *messageStorage;

@end
