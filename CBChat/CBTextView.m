//
//  CBTextView.m
//  CBChat
//
//  Created by Наиль  on 20.07.17.
//  Copyright © 2017 Наиль . All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CBTextView.h"
#import "CBTextViewDelegateProxy.h"

@interface CBTextView()<UITextViewDelegate>

@property (nonatomic, strong) UILabel *lbPlaceholder;

@property (nonatomic, strong) CBTextViewDelegatePrxoy *delegateProxy;

@end

@implementation CBTextView

#pragma mark - init methods

- (instancetype) init
{
    if ( (self = [super init]) == nil)
    {
        return nil;
    }
    self.delegateProxy = [[CBTextViewDelegatePrxoy alloc] init];
    self.delegateProxy.consumer = self;
    return self;
}

- (void) didMoveToWindow
{
    [super didMoveToWindow];
    
    self.autocorrectionType = UITextAutocorrectionTypeNo;
    self.textContainerInset = UIEdgeInsetsMake(self.textOffset.y, self.textOffset.y, 0, 0);
}

#pragma mark - UITexView ovverdies

- (void) setDelegate:(id<UITextViewDelegate>)delegate
{
    self.delegateProxy.owner = delegate;
}

- (id<UITextViewDelegate>) delegate
{
    return self.delegateProxy.consumer;
}

#pragma mark - UIView ovverides

- (void) drawRect:(CGRect)rect
{
    if (!self.lbPlaceholder)
    {
        UILabel *lb = [[UILabel alloc] init];
        lb.backgroundColor = [UIColor clearColor];
        lb.textColor = self.placeholderColor;
        lb.font = self.font;
        lb.hidden = YES;
        
        [self addSubview:lb];
        
        self.lbPlaceholder = lb;
    }
    
    self.lbPlaceholder.text = self.placeholder;
    [self.lbPlaceholder sizeToFit];
    
    self.lbPlaceholder.frame = [self prefferedPlaceholderRectWithSize:self.lbPlaceholder.frame.size];
    self.lbPlaceholder.hidden = [self prefferdPlaceholderState];
}

#pragma mark - TextViewDelegateMethods

- (BOOL)textViewShouldBeginEditing:(UITextView *)textView
{
    id<UITextViewDelegate> ownerDelegate = self.delegateProxy.owner;
    
    if ([ownerDelegate respondsToSelector:@selector(textViewShouldBeginEditing:)])
    {
        return [ownerDelegate textViewShouldBeginEditing:textView];
    }
    return YES;
}
- (BOOL)textViewShouldEndEditing:(UITextView *)textView
{
    id<UITextViewDelegate> ownerDelegate = self.delegateProxy.owner;
  
    if ([ownerDelegate respondsToSelector:@selector(textViewShouldEndEditing:)])
    {
        return [ownerDelegate textViewShouldEndEditing:textView];
    }
    return NO;
}

- (void)textViewDidBeginEditing:(UITextView *)textView
{
    id<UITextViewDelegate> ownerDelegate = self.delegateProxy.owner;
    
    if ([ownerDelegate respondsToSelector:@selector(textViewDidBeginEditing:)])
    {
        [ownerDelegate textViewDidBeginEditing:textView];
    }
}
- (void)textViewDidEndEditing:(UITextView *)textView
{
    id<UITextViewDelegate> ownerDelegate = self.delegateProxy.owner;
  
    if ([ownerDelegate respondsToSelector:@selector(textViewDidEndEditing:)])
    {
        return [ownerDelegate textViewDidEndEditing:textView];
    }
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    id<UITextViewDelegate> ownerDelegate = self.delegateProxy.owner;
    
    if ([ownerDelegate respondsToSelector:@selector(textView:shouldChangeTextInRange:replacementText:)])
    {
        return [ownerDelegate textView:textView shouldChangeTextInRange:range replacementText:text];
    }
    
    return YES;
}

- (void)textViewDidChange:(UITextView *)textView
{
    id<UITextViewDelegate> ownerDelegate = self.delegateProxy.owner;
    
    if ([ownerDelegate respondsToSelector:@selector(textViewDidChange:)])
    {
        [ownerDelegate textViewDidChange:textView];
    }
    
    self.lbPlaceholder.hidden = [self prefferdPlaceholderState];
}

- (void)textViewDidChangeSelection:(UITextView *)textView
{
    id<UITextViewDelegate> ownerDelegate = self.delegateProxy.owner;
    
    if ([ownerDelegate respondsToSelector:@selector(textViewDidChangeSelection:)])
    {
        [ownerDelegate textViewDidChangeSelection:textView];
    }
}

#pragma mark - utils methods

- (CGRect) prefferedPlaceholderRectWithSize:(CGSize)size
{
    CGRect frame = CGRectZero;
    frame.size.height = size.height;
    frame.size.width = size.width;
    frame.origin.x = self.textOffset.x;
    frame.origin.y = self.textOffset.y;
    
    return frame;
}

- (BOOL) prefferdPlaceholderState
{
    BOOL hasText = self.text.length > 0;
    BOOL hasPlacehodler = self.placeholder.length > 0;
    
    return !hasPlacehodler || hasText;
}



@end
