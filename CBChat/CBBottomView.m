

//
//  CBBottomView.m
//  CBChat
//
//  Created by Наиль  on 20.07.17.
//  Copyright © 2017 Наиль . All rights reserved.
//

#import <Foundation/Foundation.h>

#import "CBBottomView.h"
#import "CBTextView.h"

#import "NSLayoutConstraint+Utilites.h"
#import "UIColor+CustomColors.h"

@interface CBBottomView()<UITextViewDelegate>

@property (nonatomic, strong) CBTextView *textView;

@property (nonatomic, strong) UIButton *btnSend;

@end


@implementation CBBottomView

#pragma mark - init methods

- (instancetype) init
{
    if ( (self = [super init]) == nil)
    {
        return nil;
    }
    [self commonInit];
    return self;
}

- (void) commonInit
{
    CBTextView *textView = [[CBTextView alloc] init];
    textView.backgroundColor = [UIColor clearColor];
    textView.placeholder = @"Написать сообщение";
    textView.textOffset = CGPointMake(20, 16);
    textView.font = [UIFont systemFontOfSize:14];
    textView.placeholderColor = [UIColor colorWithHex:@"#8B8C9F" alpha:1.0f];
    textView.textColor = [UIColor blackColor];
    textView.translatesAutoresizingMaskIntoConstraints = NO;
    textView.delegate = self;
    
    [self addSubview:textView];
    
    [self addConstraint:[NSLayoutConstraint topFrom:self to:textView constant:0.0f]];
    [self addConstraint:[NSLayoutConstraint bottomFrom:textView to:self constant:0.0f]];
    [self addConstraint:[NSLayoutConstraint leadingFrom:textView to:self constant:0.0f]];
    
    self.textView = textView;
    
    UIButton *button = [UIButton buttonWithType:UIButtonTypeSystem];
    button.translatesAutoresizingMaskIntoConstraints = NO;
    button.enabled = NO;
    button.titleLabel.font = [UIFont systemFontOfSize:14.0f];
    button.contentHorizontalAlignment = UIControlContentHorizontalAlignmentCenter;
    button.contentVerticalAlignment = UIControlContentVerticalAlignmentBottom;
    [button setTitle:@"Отправить" forState:UIControlStateNormal];
    [button setTitleColor:[UIColor colorWithHex:@"#F7A914" alpha:1.0f] forState:UIControlStateNormal];
    [button setTitleColor:[UIColor colorWithHex:@"#8B8C9F" alpha:1.0f] forState:UIControlStateDisabled];
    [button addTarget:self action:@selector(sendAction:) forControlEvents:UIControlEventTouchUpInside];
    
    [self addSubview:button];

    [self addConstraint:[NSLayoutConstraint topFrom:button to:self constant:0.0f]];
    [self addConstraint:[NSLayoutConstraint bottomFrom:self to:button constant:16.0f]];
    [self addConstraint:[NSLayoutConstraint trailingFrom:button to:self constant:0.0f]];
    [self addConstraint:[NSLayoutConstraint fromAttribute:NSLayoutAttributeLeading from:button toAttribute:NSLayoutAttributeTrailing to:textView constant:8.0f]];
    [self addConstraint:[NSLayoutConstraint widthFor:button constant:92.0f]];
   
    self.btnSend = button;
}

- (BOOL) canBecomeFirstResponder
{
    return YES;
}

- (BOOL) becomeFirstResponder
{
    [self.textView becomeFirstResponder];
    return YES;
}

#pragma mark - UITextView delegate methods

- (void) textViewDidChange:(UITextView *)textView
{
    self.btnSend.enabled = textView.text > 0;
    
    if ([self.delegate respondsToSelector:@selector(bottomView:didChangeText:)])
    {
        [self.delegate bottomView:self didChangeText:textView.text];
    }
}

#pragma mark - action methods

- (void) sendAction:(id)sender
{
    if ([_delegate respondsToSelector:@selector(bottomViewDidSendAction:)])
    {
        [self.delegate bottomViewDidSendAction:self];
    }
    
    self.textView.text = nil;
    self.btnSend.enabled = NO;
}

@end
