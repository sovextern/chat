//
//  CBBottomView.h
//  CBChat
//
//  Created by Наиль  on 20.07.17.
//  Copyright © 2017 Наиль . All rights reserved.
//

#import <UIKit/UIKit.h>

@class CBBottomView;

@protocol CBBottomViewDelegate <NSObject>

- (void) bottomView:(CBBottomView *)view didChangeText:(NSString *)text;

- (void) bottomViewDidSendAction:(CBBottomView *)view;

@end


@class CBTextView;

@interface CBBottomView : UIView

@property (nonatomic, weak) id<CBBottomViewDelegate> delegate;

@end
