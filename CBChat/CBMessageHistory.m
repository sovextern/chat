//
//  CBMessageStory.m
//  CBChat
//
//  Created by Наиль  on 20.07.17.
//  Copyright © 2017 Наиль . All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CBMessageHistory_Private.h"
#import "CBMessage.h"
#import "CBHistoryRow.h"
#import "CBHistorySection.h"

#import "NSDate+CBUtils.h"

@interface CBMessageHistory()

@property (nonatomic, strong) NSMutableDictionary<CBMessage> *filteredMessages;

@property (nonatomic, strong) dispatch_queue_t workQueue;


@end

@implementation CBMessageHistory

#pragma mark - init methods

- (instancetype) init
{
    if ( (self = [super init]) == nil)
    {
        return nil;
    }
    
    self.workQueue = dispatch_queue_create(NULL, DISPATCH_QUEUE_SERIAL);
    self.filteredMessages = [NSMutableDictionary<CBMessage> new];
    return self;
}

#pragma mark - Publick API methods

- (void) addMessages:(NSArray<CBMessage> *)messages
{
    __weak typeof(self) weakSelf = self;
    dispatch_async(self.workQueue, ^{
        __strong typeof(weakSelf) blockSelf = weakSelf;
        [blockSelf filterMessages:messages];
    });
}

- (void) addMessage:(id<CBMessage>)message
{
    NSDate *date = [NSDate dateWithTimeIntervalSince1970:message.time];
    NSTimeInterval beginDate = [date timeIntervalSinceBeginDay];
    CBHistorySection * historySection = nil;;
    
    if ([self.filteredMessages objectForKey:@(beginDate)])
    {
        historySection = [self.filteredMessages objectForKey:@(beginDate)];
    }
    
    if ( historySection == nil)
    {
         historySection = [CBHistorySection  historySectionWithIdentifier:message.time];
         historySection.id = message.time;
    }
    
    BOOL update = [historySection addMessage:message];
    
    if (update)
    {
        [self willChangeValueForKey:@"filteredMessages"];
        [self.filteredMessages setObject:historySection forKey:@(beginDate)];
        [self didChangeValueForKey:@"filteredMessages"];
    }
}

- (NSInteger) numberOfSections
{
    return [self.filteredMessages.allKeys count];
}

- (NSInteger) numberOfItemInSection:(NSUInteger)section
{
    NSSortDescriptor *sortDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"self" ascending:YES];
    NSArray *keys = [self.filteredMessages.allKeys sortedArrayUsingDescriptors:@[sortDescriptor]];
    id key = [keys objectAtIndex:section];
    
    CBHistorySection * historySection = [self.filteredMessages objectForKey:key];
    
    return [ historySection count];
}

- (id<CBMessage>) messageAtIndex:(NSUInteger)index inSection:(NSInteger)section
{
    NSSortDescriptor *sortDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"self" ascending:YES];
    NSArray *keys = [self.filteredMessages.allKeys sortedArrayUsingDescriptors:@[sortDescriptor]];
    id key = [keys objectAtIndex:section];

    CBHistorySection* historySection = [self.filteredMessages objectForKey:key];
    CBHistoryRow* history = [ historySection historyAtIndex:index];
    
    if (!history)
    {
        return nil;
    }
    
    return history.message;
}

- (NSTimeInterval) sectionDate:(NSUInteger)section
{
    NSSortDescriptor *sortDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"self" ascending:YES];
    NSArray *keys = [self.filteredMessages.allKeys sortedArrayUsingDescriptors:@[sortDescriptor]];
    id key = [keys objectAtIndex:section];
    CBHistorySection * historySection = [self.filteredMessages objectForKey:key];
    
    return  historySection.id;
}

- (void) showedHistoryAtSection:(NSUInteger)section withIndex:(NSUInteger)index
{
    NSSortDescriptor *sortDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"self" ascending:YES];
    NSArray *keys = [self.filteredMessages.allKeys sortedArrayUsingDescriptors:@[sortDescriptor]];
    id key = [keys objectAtIndex:section];
    
    CBHistorySection* historySection = [self.filteredMessages objectForKey:key];
    CBHistoryRow* history = [ historySection historyAtIndex:index];
    history.showed = YES;
}

- (void) allIndexPathsWithCompletionBlock:(void (^)(NSArray<NSIndexPath *> *))completionBlock
{
    NSSortDescriptor *sortDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"self" ascending:YES];
    NSArray *allKyes = [[self.filteredMessages allKeys] sortedArrayUsingDescriptors:@[sortDescriptor]];
    NSMutableArray *mutable = [NSMutableArray new];
    NSIndexSet *sectionSet = [allKyes indexesOfObjectsPassingTest:^BOOL(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        return YES;
    }];
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        
        NSUInteger currentSection = [sectionSet firstIndex];
        while (currentSection != NSNotFound)
        {
            id key = [allKyes objectAtIndex:currentSection];
            CBHistorySection * historySection = [self.filteredMessages objectForKey:key];
            NSIndexSet *rowSet = [ historySection allMessagesIndexs];
            NSUInteger currentRow = [rowSet firstIndex];
            while (currentRow != NSNotFound)
            {
                NSIndexPath *indexPath = [NSIndexPath indexPathForRow:currentRow inSection:currentSection];
                [mutable addObject:indexPath];
                currentRow = [rowSet indexGreaterThanIndex:currentRow];
            }
            currentSection = [sectionSet indexGreaterThanIndex:currentSection];
        }
        
        if (completionBlock)
        {
            completionBlock([mutable copy]);
        }
    });
}

#pragma mark - utils

- (void) filterMessages:(NSArray<CBMessage> *)messages
{
    if (messages == nil)
    {
        return;
    }
    
    @synchronized (self)
    {
        NSSortDescriptor *sortDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"time" ascending:YES];
        NSArray *sotredMessages = [messages sortedArrayUsingDescriptors:@[sortDescriptor]];
        NSMutableDictionary *dict = [self.filteredMessages mutableCopy];
        
        for (NSInteger i = 0; i<sotredMessages.count; i++)
        {
            id<CBMessage> message = [sotredMessages objectAtIndex:i];
            NSDate *date = [NSDate dateWithTimeIntervalSince1970:message.time];
            NSTimeInterval beginDayInterval = [date timeIntervalSinceBeginDay];
            
            NSPredicate *predicate = [self predicateFiltedByTime:beginDayInterval inPeriod:86400];
            NSMutableArray<CBMessage> *periodMessages = [[sotredMessages filteredArrayUsingPredicate:predicate] mutableCopy];
            
            
            NSLog(@"%@",date);
            
            
            CBHistorySection * historySection = [self makeHistorySectionWithMessages:periodMessages withIdentifier:message.time];;
            
            if ([self.filteredMessages objectForKey:@(beginDayInterval)])
            {
                historySection = [self.filteredMessages objectForKey:@(beginDayInterval)];
                [historySection addMessages:periodMessages];
            }
            
            [dict setObject:historySection forKey:@(beginDayInterval)];
            
            i += periodMessages.count;
        }
        
        [self willChangeValueForKey:@"filteredMessages"];
        self.filteredMessages = dict;
        [self didChangeValueForKey:@"filteredMessages"];
    }
}

- (NSString *) stringDateFromTimeInterval:(NSTimeInterval)interval inFormat:(NSString *)format
{
    NSDate *date = [NSDate dateWithTimeIntervalSince1970:interval];
    NSDateFormatter *dateFormmater = [[NSDateFormatter alloc] init];
    dateFormmater.dateFormat=format;
    dateFormmater.timeZone = [NSTimeZone timeZoneWithAbbreviation:@"UTC"];
    
    return [dateFormmater stringFromDate:date];
}

- (NSPredicate *) predicateFiltedByTime:(NSTimeInterval)interval inPeriod:(NSTimeInterval)period
{
    return [NSPredicate predicateWithBlock:^BOOL(id  _Nullable evaluatedObject, NSDictionary<NSString *,id> * _Nullable bindings) {
        id<CBMessage> message = evaluatedObject;
        NSTimeInterval time = message.time;
        return (time-interval) < period && (time-interval) >= 0;
    }];
}

- (CBHistorySection *) makeHistorySectionWithMessages:(NSArray<CBMessage> *)messages withIdentifier:(NSUInteger)id
{
    return [CBHistorySection historySectionWithMessages:messages withIdentifier:id];
}

@end
