//
//  CBLayout.h
//  CBChat
//
//  Created by Наиль  on 21.07.17.
//  Copyright © 2017 Наиль . All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CBLayout : NSObject

@property (nonatomic, assign) CGSize size;

@property (nonatomic, assign) CGPoint offset;

@property (nonatomic, assign) NSTextAlignment timeAligment;

@property (nonatomic, assign) NSTextAlignment textAligment;

@property (nonatomic, strong) UIColor *textColor;

@property (nonatomic, strong) UIColor *timeColor;

@property (nonatomic, strong) UIColor *backgroundColor;

@end
