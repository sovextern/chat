//
//  CBMessageLayout.h
//  CBChat
//
//  Created by Наиль  on 21.07.17.
//  Copyright © 2017 Наиль . All rights reserved.
//


#define CellMaxWidth 260.0f
#define CellMinHeight 66.0f
#define CellMinWidth 56.0f

#import <Foundation/Foundation.h>

@class CBLayout;
@protocol CBMessage;

@interface CBMessageLayout : NSObject

- (instancetype) initWithFrame:(CGRect)frame andFont:(UIFont *)font;

- (CBLayout *) layoutForMessage:(id<CBMessage>)message;

@end
