//
//  CBHistoryArray.h
//  CBChat
//
//  Created by Наиль  on 24.07.17.
//  Copyright © 2017 Наиль . All rights reserved.
//

@class CBHistoryRow;
@protocol CBMessage;


@interface CBHistorySection : NSObject

@property (nonatomic, assign) NSUInteger id;

+ (id) historySectionWithMessages:(NSArray<CBMessage> *)messages withIdentifier:(NSUInteger)identifier;

+ (id) historySectionWithIdentifier:(NSUInteger)identifier;

- (BOOL) addMessage:(id<CBMessage>)message;

- (void) addMessages:(NSArray<CBMessage> *)messages;

- (CBHistoryRow *) historyAtIndex:(NSUInteger)index;

- (BOOL) isAllShowed;

- (NSIndexSet *) allMessagesIndexs;

- (NSUInteger) count;

@end
