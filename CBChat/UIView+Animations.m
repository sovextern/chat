//
//  UIView+Animations.m
//  CreditCalendar
//
//  Created by sovcombank on 08.09.16.
//  Copyright © 2016 Alef. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "UIView+Animations.h"

@implementation UIView (Animations)


#pragma mark - hide/show standart animations

- (void) animatedShowWithDuration:(NSTimeInterval)duration alpha:(CGFloat)alpha
{
    [self animatedShowWithDuration:duration alpha:alpha animationBlock:nil completionBlock:nil];
}

- (void) animatedShowWithDuration:(NSTimeInterval)duration alpha:(CGFloat)alpha withCompletionBlock:(void (^)(void))completionBlock
{
    [self animatedShowWithDuration:duration alpha:alpha animationBlock:nil completionBlock:completionBlock];
}

- (void) animatedShowWithDuration:(NSTimeInterval)duration alpha:(CGFloat)alpha animationBlock:(void(^)(void))animationBlock
{
     [self animatedShowWithDuration:duration alpha:alpha animationBlock:animationBlock completionBlock:nil];
}

- (void) animatedShowWithDuration:(NSTimeInterval)duration alpha:(CGFloat)alpha animationBlock:(void(^)(void))animationBlock completionBlock:(void(^)(void))completionBlock
{
    self.alpha = 0;
    self.hidden = NO;

    [UIView animateWithDuration:duration animations:^{
        self.alpha = alpha;
        if (animationBlock != nil)
        {
            animationBlock();
        }
    } completion:^(BOOL finished) {
        #pragma unused(finished)
        if (completionBlock != nil)
        {
            completionBlock();
        }
    }];
}


- (void) animatedHideWithDuration:(NSTimeInterval)duration
{
    [self animatedHideWithDuration:duration animationBlock:nil withCompletionBlock:nil];
}

- (void) animatedHideWithDuration:(NSTimeInterval)duration withCompletionBlock:(void (^)(void))completionBlock
{
    [self animatedHideWithDuration:duration animationBlock:nil withCompletionBlock:completionBlock];
}

- (void) animatedHideWithDuration:(NSTimeInterval)duration animationBlock:(void(^)(void))animationBlock
{
    [self animatedHideWithDuration:duration animationBlock:animationBlock withCompletionBlock:nil];
}

- (void) animatedHideWithDuration:(NSTimeInterval)duration animationBlock:(void(^)(void))animationBlock withCompletionBlock:(void(^)(void))completionBlock
{
    [UIView animateWithDuration:duration animations:^{
        self.alpha = 0;
        
        if (animationBlock != nil)
        {
            animationBlock();
        }
    } completion:^(BOOL finished) {
#pragma unused(finished)
        self.hidden = YES;
        if (completionBlock != nil)
        {
            completionBlock();
        }
    }];

}

#pragma mark - constrain standart animations

- (void) animatedChangeConstrain:(NSLayoutConstraint *)constrain withDuration:(NSTimeInterval)duration constant:(CGFloat)constant
{
    [self animatedChangeConstrain:constrain withDuration:duration constant:constant animationBlock:nil completionBlock:nil];
}

- (void) animatedChangeConstrain:(NSLayoutConstraint *)constrain withDuration:(NSTimeInterval)duration constant:(CGFloat)constant animationBlock:(void (^)(void))animationBlock
{
    [self animatedChangeConstrain:constrain withDuration:duration constant:constant animationBlock:animationBlock completionBlock:nil];
}

- (void) animatedChangeConstrain:(NSLayoutConstraint *)constrain withDuration:(NSTimeInterval) duration constant:(CGFloat) constant completionBlock:(void(^)(void))completionBlock
{
    [self animatedChangeConstrain:constrain withDuration:duration constant:constant animationBlock:nil completionBlock:completionBlock];
}

- (void) animatedChangeConstrain:(NSLayoutConstraint *)constrain withDuration:(NSTimeInterval)duration constant:(CGFloat)constant animationBlock:(void (^)(void))animationBlock completionBlock:(void (^)(void))completionBlock
{
    [self layoutIfNeeded];
    constrain.constant = constant;
    [UIView animateWithDuration:duration animations:^{
        [self layoutIfNeeded];
        if (animationBlock != nil)
        {
            animationBlock();
        }
        
    } completion:^(BOOL finished) {
#pragma unused(finished)
        if (completionBlock != nil)
        {
            completionBlock();
        }
    }];
}



@end

