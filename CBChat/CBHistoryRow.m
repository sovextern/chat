//
//  CBHistory.m
//  CBChat
//
//  Created by Наиль  on 24.07.17.
//  Copyright © 2017 Наиль . All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CBHistoryRow.h"

@implementation CBHistoryRow

- (BOOL) isEqual:(id)object
{
    CBHistoryRow *other = object;
    
    return other.id == self.id;
}

@end
