//
//  CBHexKey.h
//  CBChat
//
//  Created by Наиль  on 21.07.17.
//  Copyright © 2017 Наиль . All rights reserved.
//

@interface CBHashKey : NSObject<NSCopying>

+ (id) keyWithHash:(NSUInteger)hash;

@end
