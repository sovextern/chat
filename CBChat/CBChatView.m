
//
//  CBChatView.m
//  CBChat
//
//  Created by Наиль  on 20.07.17.
//  Copyright © 2017 Наиль . All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CBChatView.h"
#import "CBBottomView.h"
#import "CBMessageCellView.h"
#import "CBMessageHeaderView.h"
#import "CBCollectionView.h"

#import "CBMessageHistory_Private.h"
#import "CBMessageStorage_Private.h"
#import "CBMessage.h"

#import "CBLayout.h"
#import "CBCollectionFlowLayout.h"
#import "CBMessageLayout.h"

#import "NSLayoutConstraint+Utilites.h"
#import "UIColor+CustomColors.h"
#import "NSDate+CBUtils.h"
#import "UIView+Animations.h"

static NSString *const kCBChatCellId = @"CBChatCellId";
static NSString *const kCBChatHeaderId = @"CBChatHeaderId";

@interface CBChatView()<UICollectionViewDelegate,UICollectionViewDataSource,UIGestureRecognizerDelegate,CBMesssageStorageDelegate_Private,CBCollectionFlowLayoutDelegate,CBBottomViewDelegate>

@property (nonatomic, strong) CBBottomView *bottomView;
@property (nonatomic, strong) CBCollectionView *collectionView;

@property (nonatomic, strong) NSLayoutConstraint *bottomViewB;

@property (nonatomic, strong) CBMessageLayout *messageLayout;
@property (nonatomic, strong) CBMessage *bufferMessage;

@property (nonatomic, strong) UITapGestureRecognizer *tap;
@property (nonatomic, strong) dispatch_queue_t workQueue;
@property (nonatomic, strong) dispatch_semaphore_t reloadSemaphore;

@end

@implementation CBChatView

#pragma mark - init methods

+ (void) initialize
{
    CBChatView *a = [CBChatView appearance];
    a.tempDefaultFont = [UIFont systemFontOfSize:14];
}

- (instancetype) initWithCoder:(NSCoder *)aDecoder
{
    if ( (self = [super initWithCoder:aDecoder]) == nil)
    {
        return nil;
    }
    [self commonInit];
    return self;
}

- (instancetype) initWithFrame:(CGRect)frame
{
    if ( ( self = [super initWithFrame:frame]) == nil)
    {
        return nil;
    }
    [self commonInit];
    return self;
}

- (instancetype) init
{
    if ( (self = [super init]) == nil)
    {
        return nil;
    }
    [self commonInit];
    return self;
}

- (void) commonInit
{
    self.bufferMessage = nil;
    self.workQueue = dispatch_queue_create(NULL, DISPATCH_QUEUE_SERIAL);
    self.reloadSemaphore = dispatch_semaphore_create(0);
    
    CBBottomView *bottomView = [[CBBottomView alloc] init];
    bottomView.delegate = self;
    bottomView.translatesAutoresizingMaskIntoConstraints = NO;
    
    [self addSubview:bottomView];
    
    self.bottomViewB = [NSLayoutConstraint bottomFrom:self to:bottomView constant:0.0f];
    
    [self addConstraint:[NSLayoutConstraint leadingFrom:bottomView to:self constant:0.0f]];
    [self addConstraint:[NSLayoutConstraint trailingFrom:self to:bottomView constant:0.0f]];
    [self addConstraint:self.bottomViewB];
    [self addConstraint:[NSLayoutConstraint heightFor:bottomView constant:77.0f]];
    
    self.bottomView  = bottomView;
    
    CBCollectionFlowLayout *flowLayout = [[CBCollectionFlowLayout alloc] init];
    flowLayout.scrollDirection = UICollectionViewScrollDirectionVertical;
    flowLayout.delegate = self;
    
    CBCollectionView *collectionView = [[CBCollectionView alloc] initWithFrame:CGRectZero collectionViewLayout:flowLayout];
    collectionView.translatesAutoresizingMaskIntoConstraints = NO;
    collectionView.keyboardDismissMode = UIScrollViewKeyboardDismissModeNone;
    collectionView.backgroundColor = [UIColor colorWithHex:@"#303154" alpha:1.0f];
    collectionView.delegate = self;
    collectionView.dataSource = self;
    
    [collectionView registerClass:[CBMessageCellView class] forCellWithReuseIdentifier:kCBChatCellId];
    [collectionView registerClass:[CBMessageHeaderView class] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:kCBChatHeaderId];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapAction:)];
    tap.numberOfTapsRequired = 1;
    tap.numberOfTouchesRequired = 1;
    tap.delegate = self;
    
    [collectionView addGestureRecognizer:tap];
    self.tap = tap;
    
    [self addSubview:collectionView];
    
    [self addConstraint:[NSLayoutConstraint leadingFrom:self to:collectionView constant:0.0f]];
    [self addConstraint:[NSLayoutConstraint trailingFrom:collectionView to:self constant:0.0f]];
    [self addConstraint:[NSLayoutConstraint topFrom:collectionView to:self constant:0.0f]];
    [self addConstraint:[NSLayoutConstraint fromAttribute:NSLayoutAttributeBottom from:collectionView topTo:bottomView constant:0.0f]];
    
    self.collectionView = collectionView;

    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(notificationWillShowKeyboard:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(notificationWillHideKeyboard:) name:UIKeyboardWillHideNotification object:nil];
}

- (CBMessageLayout *) messageLayout
{
    if (_messageLayout)
    {
        return _messageLayout;
    }
    
    _messageLayout = [[CBMessageLayout alloc] initWithFrame:self.bounds andFont:self.tempDefaultFont];
    return _messageLayout;
}

- (void) dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    
    NSLog(@"%@ deallocated",NSStringFromClass(self.class));
}

- (BOOL)canBecomeFirstResponder
{
    return YES;
}

- (BOOL) becomeFirstResponder
{
    [self.bottomView becomeFirstResponder];
    return YES;
}

#pragma mark - notifications handler methods

- (void) notificationWillShowKeyboard:(NSNotification *)notif
{
    NSDictionary *info = [notif userInfo];
    CGSize kbSize = [[info objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].size;
    NSTimeInterval duration = [info[UIKeyboardAnimationDurationUserInfoKey] doubleValue];
    
    [self animatedChangeConstrain:self.bottomViewB withDuration:duration constant:kbSize.height];
}

- (void) notificationWillHideKeyboard:(NSNotification *)notif
{
    NSDictionary *info = [notif userInfo];
    NSTimeInterval duration = [info[UIKeyboardAnimationDurationUserInfoKey] doubleValue];
    
    [self animatedChangeConstrain:self.bottomViewB withDuration:duration constant:0.0f];
}

#pragma mark - UICollectionViewDelegate protocol methods

- (void) collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    
}

#pragma mark - UICollectionViewDataSource protocol methods

- (NSInteger) numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    if (self.messageStorage)
    {
        return [self.messageStorage.history numberOfSections];
    }
    
    return 0;
}

- (NSInteger) collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    if (self.messageStorage)
    {
        return [self.messageStorage.history numberOfItemInSection:section];
    }
    return 0;
}

- (__kindof UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    CBMessageCellView *cell = [collectionView dequeueReusableCellWithReuseIdentifier:kCBChatCellId forIndexPath:indexPath];
    cell.font = self.tempDefaultFont;
    
    CBMessageHistory *history = self.messageStorage.history;
    id<CBMessage> message = [history messageAtIndex:indexPath.row inSection:indexPath.section];
    CBLayout *layout = [self.messageLayout layoutForMessage:message];
    
    [cell setupLayout:layout];
    [cell configWithMessage:message];
    
    [history showedHistoryAtSection:indexPath.section withIndex:indexPath.row];
    return cell;
}

- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath
{
    UICollectionReusableView *reusableView = nil;
    if (kind == UICollectionElementKindSectionHeader)
    {
        CBMessageHistory *history = self.messageStorage.history;
        NSTimeInterval time = [history sectionDate:indexPath.section];
        NSString *dateString = [NSDate stringDateFromTimeInterval:time inFormat:@"dd.MM.yyyy" timeZone:nil];
        
        CBMessageHeaderView *headerView = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:kCBChatHeaderId forIndexPath:indexPath];
        headerView.backgroundColor = [UIColor clearColor];
    
        NSDate *date = [[NSDate dateWithTimeIntervalSince1970:time] toLocalTime];
        if ([date isToday])
        {
            headerView.lbText.text = @"Сегодня";
        } else {
            headerView.lbText.text = dateString;
        }
        
        reusableView = headerView;
    }

    return reusableView;
}

#pragma mark - UIGestureRecognizerDelegate protocol methods

- (BOOL) gestureRecognizerShouldBegin:(UIGestureRecognizer *)gestureRecognizer
{
    return gestureRecognizer == self.tap;
}

#pragma mark - CBCollectionFlowLayoutDelegate protocol methods

- (CGPoint) collectionView:(UICollectionView *)collectionView cellOffsetAtIndexPath:(NSIndexPath *)indexPath
{
    NSInteger index = indexPath.row;
    NSInteger section = indexPath.section;
    
    id<CBMessage> message = [self.messageStorage messageInSection:section atIndex:index];
    CBLayout *layout = [self.messageLayout layoutForMessage:message];
    
    return layout.offset;
}

- (CGSize) collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    NSInteger index = indexPath.row;
    NSInteger section = indexPath.section;
    
    id<CBMessage> message = [self.messageStorage messageInSection:section atIndex:index];
    CBLayout *layout = [self.messageLayout layoutForMessage:message];

    return layout.size;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section;
{
    CGFloat width = CGRectGetWidth(self.frame);
    
    return CGSizeMake(width, 12);
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
    return UIEdgeInsetsMake(10, 0, -20, 0);
}

#pragma mark - Action methods

- (void) tapAction:(id)sender
{
    [self endEditing:YES];
}

#pragma mark - Publick API methods

- (void) setMessageStorage:(CBMessageStorage *)messageStorage
{
    if (self.messageStorage)
    {
        self.messageStorage.pDelegate = nil;
    }
    
    _messageStorage = messageStorage;
    _messageStorage.pDelegate = self;
}

#pragma mark - CBMesssageStorageDelegate_Private protocol methods

- (void) messageStorageCompleteBuildPartOfMessages:(CBMessageStorage *)storage
{
    CBMessageHistory *history = storage.history;
    
//    if ([history numberOfSections] == [self.collectionView numberOfSections])
//    {
//        __weak typeof(self) weakSelf = self;
//        [history allIndexPathsWithCompletionBlock:^(NSArray<NSIndexPath *> *indexPaths) {
//            __strong typeof(weakSelf) blockSelf = weakSelf;
//            
//            if (indexPaths.count == 0)
//            {
//                return;
//            }
//            
//            NSArray *filteredIndexPaths = [self.collectionView filterInsertedIndexPaths:indexPaths];
//            
//            if (filteredIndexPaths.count == 0)
//            {
//                return;
//            }
//            
//            dispatch_sync(dispatch_get_main_queue(), ^{
//                
//                [blockSelf.collectionView performBatchUpdates:^{
//                    [blockSelf.collectionView insertItemsAtIndexPaths:filteredIndexPaths];
//                } completion:^(BOOL finished) {
//                 //  [blockSelf.collectionView scrollToBottom];
//                }];
//            });
//        }];
//        return;
//    }
    
     if ([NSThread isMainThread])
     {
         [self.collectionView reloadData];
         [self.collectionView.collectionViewLayout invalidateLayout];
         return;
     }
    
    __weak typeof(self) weakSelf = self;
    
         dispatch_async(dispatch_get_main_queue(), ^{
            __strong typeof(weakSelf) blockSelf = weakSelf;
             [blockSelf.collectionView reloadData];
        });
}

#pragma mark - CBBottomViewDelegate protocol methods

- (void) bottomView:(CBBottomView *)view didChangeText:(NSString *)text
{
    if (!self.bufferMessage)
    {
        self.bufferMessage = [[CBMessage alloc] init];
        self.bufferMessage.staff = NO;
    }
    
    self.bufferMessage.text = text;
}

- (void) bottomViewDidSendAction:(CBBottomView *)view
{
    if (!self.bufferMessage)
    {
        return;
    }
    
    self.bufferMessage.time = [[NSDate date] timeIntervalSince1970];
    [self.messageStorage addMessage:self.bufferMessage];
    
    id<CBMessageStorageDelegate> delegate = self.messageStorage.delegate;
    
    if ([delegate respondsToSelector:@selector(messageStorageAddNewMessage:)])
    {
        [delegate messageStorageAddNewMessage:self.bufferMessage];
    }
    
    self.bufferMessage = nil;
}

@end
