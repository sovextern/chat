//
//  CBMessageStorage.h
//  CBChat
//
//  Created by Наиль  on 20.07.17.
//  Copyright © 2017 Наиль . All rights reserved.
//

#import <UIKit/UIKit.h>

@class CBMessageHistory,CBLayout;
@protocol CBMessage;

@protocol CBMessageStorageDelegate <NSObject>

- (void) messageStorageAddNewMessage:(id<CBMessage>)message;

@end

@interface CBMessageStorage : NSObject

@property (nonatomic, strong, readonly) CBMessageHistory *history;

@property (nonatomic, weak) id<CBMessageStorageDelegate> delegate;

- (void) addMessage:(id<CBMessage>)message;

- (void) addMessages:(NSArray<CBMessage> *)messages;

- (id<CBMessage>) messageInSection:(NSInteger)section atIndex:(NSInteger)index;


@end
