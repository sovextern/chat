//
//  ViewController.m
//  CBChat
//
//  Created by Наиль  on 20.07.17.
//  Copyright © 2017 Наиль . All rights reserved.
//

#import "ViewController.h"
#import "CBMessageStorage.h"
#import "CBMessage.h"
#import "CBChatView.h"

#import "NSDate+CBUtils.h"

@interface ViewController ()

@property (nonatomic, strong) CBMessageStorage *storage;
@property (weak, nonatomic) IBOutlet CBChatView *chatView;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    CBMessage *m1 = [[CBMessage alloc] init];
    m1.text = @"Добрый день! У меня дублируются";
    m1.staff = YES;
    m1.time = [[NSDate date] timeIntervalSince1970] ;
    
    CBMessage *m2 = [[CBMessage alloc] init];
    m2.text = @"Добрый день! У меня дублируются";
    m2.staff = NO;
    m2.time = [[NSDate date] timeIntervalSince1970] + 1000;
    
    
    self.storage = [[CBMessageStorage alloc] init];
    [self.storage addMessages:@[m1,m2]];
    
    self.chatView.messageStorage = self.storage;
    // Do any additional setup after loading the view, typically from a nib.
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
