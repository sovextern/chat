//
//  UIView+Animations.h
//  CreditCalendar
//
//  Created by sovcombank on 08.09.16.
//  Copyright © 2016 Alef. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (Animations)

/// animation on constraints

- (void) animatedChangeConstrain:(NSLayoutConstraint *)constrain withDuration:(NSTimeInterval) duration constant:(CGFloat) constant animationBlock:(void(^)(void))animationBlock completionBlock:(void(^)(void))completionBlock;

- (void) animatedChangeConstrain:(NSLayoutConstraint *)constrain withDuration:(NSTimeInterval) duration constant:(CGFloat) constant;

- (void) animatedChangeConstrain:(NSLayoutConstraint *)constrain withDuration:(NSTimeInterval) duration constant:(CGFloat) constant completionBlock:(void(^)(void))completionBlock;

- (void) animatedChangeConstrain:(NSLayoutConstraint *)constrain withDuration:(NSTimeInterval) duration constant:(CGFloat) constant animationBlock:(void(^)(void))animationBlock;


/// animations on alpha

- (void) animatedShowWithDuration:(NSTimeInterval)duration alpha:(CGFloat)alpha animationBlock:(void(^)(void))animationBlock completionBlock:(void(^)(void))completionBlock;

- (void) animatedShowWithDuration:(NSTimeInterval)duration alpha:(CGFloat)alpha;

- (void) animatedShowWithDuration:(NSTimeInterval)duration alpha:(CGFloat)alpha withCompletionBlock:(void(^)(void))completionBlock;

- (void) animatedShowWithDuration:(NSTimeInterval)duration alpha:(CGFloat)alpha animationBlock:(void(^)(void))animationBlock;


- (void) animatedHideWithDuration:(NSTimeInterval) duration;

- (void) animatedHideWithDuration:(NSTimeInterval) duration withCompletionBlock:(void(^)(void))completionBlock;

- (void) animatedHideWithDuration:(NSTimeInterval)duration animationBlock:(void(^)(void))animationBlock;

- (void) animatedHideWithDuration:(NSTimeInterval)duration animationBlock:(void(^)(void))animationBlock withCompletionBlock:(void(^)(void))completionBlock;


@end
