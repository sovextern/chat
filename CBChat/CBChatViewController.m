//
//  CBChatViewController.m
//  CBChat
//
//  Created by Наиль  on 20.07.17.
//  Copyright © 2017 Наиль . All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CBChatViewController.h"

@interface CBChatViewController()

@end

@implementation CBChatViewController

#pragma mark - init methods

- (instancetype) initWithCoder:(NSCoder *)aDecoder
{
    if ( (self = [super initWithCoder:aDecoder]) == nil)
    {
        return nil;
    }
    
    [self commonInit];
    return self;
}

- (void) commonInit
{
    
}

@end
