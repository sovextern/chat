//
//  CBTextViewProxy.h
//  CBChat
//
//  Created by Наиль  on 20.07.17.
//  Copyright © 2017 Наиль . All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface CBTextViewDelegatePrxoy: NSObject

@property (nonatomic, weak) id<UITextViewDelegate> consumer;

@property (nonatomic, weak) id<UITextViewDelegate> owner;

@end
