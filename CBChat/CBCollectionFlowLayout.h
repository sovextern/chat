//
//  CBCollectionFlowLayout.h
//  CBChat
//
//  Created by Наиль  on 20.07.17.
//  Copyright © 2017 Наиль . All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol  CBCollectionFlowLayoutDelegate <UICollectionViewDelegateFlowLayout>

- (CGPoint) collectionView:(UICollectionView *)collectionView cellOffsetAtIndexPath:(NSIndexPath *)indexPath;


@end


@interface CBCollectionFlowLayout : UICollectionViewFlowLayout

@property (nonatomic, weak) id<CBCollectionFlowLayoutDelegate> delegate;


@end
