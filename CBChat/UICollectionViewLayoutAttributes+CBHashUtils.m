//
//  UICollectionViewLayoutAttributes+CBHashUtils.m
//  CBChat
//
//  Created by Наиль  on 28.07.17.
//  Copyright © 2017 Наиль . All rights reserved.
//

#import <Foundation/Foundation.h>
#import "UICollectionViewLayoutAttributes+CBHashUtils.h"

@implementation UICollectionViewLayoutAttributes(CBHashUtils)

- (NSUInteger) hashByKindAndIndexPath
{
    NSIndexPath *indexPath = self.indexPath;
    NSString *kind = self.representedElementKind;
    
    return [kind hash] + [indexPath hash];
}


@end
