//
//  CBMessageLayout.m
//  CBChat
//
//  Created by Наиль  on 21.07.17.
//  Copyright © 2017 Наиль . All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreText/CoreText.h>
#import <UIKit/UIKit.h>

#import "CBMessageLayout.h"
#import "CBMessage.h"

#import "CBHashKey.h"
#import "CBLayout.h"

#import "UIColor+CustomColors.h"

@interface CBMessageLayout()

@property (nonatomic, strong) UIFont *font;

@property (nonatomic, assign) CGRect rect;

@property (nonatomic, strong) NSMutableDictionary *layoutCache;

@end

@implementation CBMessageLayout

- (instancetype) initWithFrame:(CGRect)frame andFont:(UIFont *)font
{
    if ( ( self = [super init]) == nil)
    {
        return nil;
    }
    
    self.font = font;
    self.rect = frame;
    self.layoutCache = [[NSMutableDictionary alloc] init];
    return self;
}

#pragma mark - Publick API methods

- (CBLayout *) layoutForMessage:(id<CBMessage>)message
{
    NSUInteger messageHash = [self hashForMessage:message];
    CBHashKey *hashKey = [CBHashKey keyWithHash:messageHash];
    
    if ([self.layoutCache objectForKey:hashKey])
    {
        return [self.layoutCache objectForKey:hashKey];
    }
    
    CBLayout *layout = [[CBLayout alloc] init];
    layout.textColor = [UIColor whiteColor];
    layout.timeColor = [[UIColor whiteColor] colorWithAlphaComponent:0.5];
    layout.size = [self sizeForMessage:message];
    
    BOOL isStaff = message.isStaff;
    
    if (isStaff)
    {
        layout.offset = CGPointMake(10, 14);
        layout.backgroundColor = [UIColor colorWithHex:@"#595A76" alpha:1.0f];
        layout.timeAligment = NSTextAlignmentRight;
        layout.textAligment = NSTextAlignmentLeft;
        
        [self.layoutCache setObject:layout forKey:(id)hashKey];
        
        return layout;
    }
    
    CGFloat width = CGRectGetWidth(self.rect);

    layout.offset = CGPointMake(width-layout.size.width-10, 14);
    layout.backgroundColor = [UIColor colorWithHex:@"#F7A914" alpha:1.0f];
    layout.timeAligment = NSTextAlignmentLeft;
    layout.textAligment = NSTextAlignmentLeft;
    
    [self.layoutCache setObject:layout forKey:(id)hashKey];
    
    return layout;
}

#pragma mark - utils methods

- (NSUInteger) hashForMessage:(id<CBMessage>)message
{
    NSUInteger textHash = [message.text length];
    return textHash+message.time;
}

#pragma mark - Private API methods


#pragma mark - utils methods

- (CGSize) sizeForMessage:(id<CBMessage>)message
{
    CGFloat tW = [self calculateSizeForText:message.text atSize:CGSizeMake(FLT_MAX, _font.lineHeight) forFont:_font].width + 24.0f;
    
    if (tW > CellMaxWidth)
    {
        tW = CellMaxWidth;
    }  else if (tW < CellMinWidth)
    {
        tW = CellMinWidth;
    }
    
   
    CGFloat tH = [self calculateSizeForText:message.text atSize:CGSizeMake(tW, FLT_MAX) forFont:_font].height;
    return CGSizeMake(tW, tH+47.0f);
//    CGFloat textHeight = [self calculateSizeForText:message.text atSize:CGSizeMake(CellMaxWidth-24.0f, FLT_MAX) forFont:self.font].height;
//    CGFloat boxHeight = textHeight +46.0f;
//    
//    if (boxHeight > CellMinHeight)
//    {
//        return CGSizeMake(CellMaxWidth, boxHeight);
//    }
//    
//    CGFloat textWidth = [self calculateSizeForText:message.text atSize:CGSizeMake(FLT_MAX, textHeight) forFont:self.font].width ;
//    CGFloat boxWidth = textWidth + 24.0f;
//    
//    if (boxWidth < CellMinWidth)
//    {
//        return CGSizeMake(CellMinWidth, boxHeight);
//    }
//    
//    return CGSizeMake(boxWidth, boxHeight);
}

- (CGSize) calculateSizeForText:(NSString *)text atSize:(CGSize)size forFont:(UIFont *)font
{
    if (!text)
    {
        return CGSizeZero;
    }
    
    if (!font)
    {
        return CGSizeZero;
    }
    
//    NSAttributedString *attrStr = [[NSAttributedString alloc] initWithString:text attributes:@{NSFontAttributeName:font}];
//    
//    CFAttributedStringRef cfAttrString = CFBridgingRetain(attrStr);
//    CTFramesetterRef framesetter = CTFramesetterCreateWithAttributedString(cfAttrString);
//    CGSize textSize = CTFramesetterSuggestFrameSizeWithConstraints(framesetter, CFRangeMake(0, text.length), nil, size, nil);
//    
//    CFRelease(cfAttrString);
//    CFRelease(framesetter);
//    
//    return textSize;
//    
    
    NSTextStorage *textStorage = [[NSTextStorage alloc] initWithString:text];
    NSTextContainer  *textContainter = [[NSTextContainer alloc] initWithSize:size];
    NSLayoutManager *layoutManager = [[NSLayoutManager alloc] init];
   
    [layoutManager addTextContainer:textContainter];
    [textStorage addLayoutManager:layoutManager];
    [textStorage addAttribute:NSFontAttributeName value:font range:NSMakeRange(0, [text length])];
    [textContainter setLineFragmentPadding:10.0f];
    [textContainter setLineBreakMode:NSLineBreakByWordWrapping];
    
    NSRange glyphRange = [layoutManager glyphRangeForTextContainer:textContainter];
    [layoutManager invalidateDisplayForGlyphRange:glyphRange];
    CGRect frame = [layoutManager usedRectForTextContainer:textContainter];
    return frame.size;
}





@end
