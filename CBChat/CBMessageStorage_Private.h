//
//  CBMessageStorage_Private.h
//  CBChat
//
//  Created by Наиль  on 21.07.17.
//  Copyright © 2017 Наиль . All rights reserved.
//

#import "CBMessageStorage.h"

@protocol CBMesssageStorageDelegate_Private <NSObject>

- (void) messageStorageCompleteBuildPartOfMessages:(CBMessageStorage *)storage;


@end

@interface CBMessageStorage()

@property (nonatomic, weak) id<CBMesssageStorageDelegate_Private> pDelegate;

@end
