
//
//  CBMessageHeaderView.m
//  CBChat
//
//  Created by Наиль  on 21.07.17.
//  Copyright © 2017 Наиль . All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CBMessageHeaderView.h"

#import "UIColor+CustomColors.h"
#import "NSLayoutConstraint+Utilites.h"

@implementation CBMessageHeaderView

- (instancetype) init
{
    if ( (self = [super init]) == nil)
    {
        return nil;
    }
    [self commonInit];
    return self;
}

- (instancetype) initWithFrame:(CGRect)frame
{
    if ( (self = [super initWithFrame:frame]) == nil)
    {
        return nil;
    }
    [self commonInit];
    return self;
}

- (void) commonInit
{
    UILabel *lbText = [[UILabel alloc] init];
    lbText.textColor = [UIColor colorWithHex:@"#969AA5" alpha:1.0f];
    lbText.font = [UIFont systemFontOfSize:13.0f];
    lbText.textAlignment = NSTextAlignmentCenter;
    lbText.translatesAutoresizingMaskIntoConstraints = NO;
    
    [self addSubview:lbText];
    
    [self addConstraints:[NSLayoutConstraint standartBoxConstratinsFrom:self to:lbText constant:0.0f]];
    
    self.lbText = lbText;
}

@end
