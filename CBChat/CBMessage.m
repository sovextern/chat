//
//  CBMessage.m
//  CBChat
//
//  Created by Наиль  on 20.07.17.
//  Copyright © 2017 Наиль . All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CBMessage.h"

@implementation CBMessage

@synthesize text = _text;
@synthesize time = _time;
@synthesize staff = _staff;

@end
