//
//  NSDate+Utils.h
//  CBChat
//
//  Created by Наиль  on 21.07.17.
//  Copyright © 2017 Наиль . All rights reserved.
//

#import <Foundation/Foundation.h>


@interface NSDate(CBUtils)

+ (NSString *) stringDateFromTimeInterval:(NSTimeInterval)interval inFormat:(NSString *)format timeZone:(NSTimeZone *)timeZone;

- (NSTimeInterval) timeIntervalSinceBeginDay;

- (BOOL) isToday;

-(NSDate *) toLocalTime;

-(NSDate *) toGlobalTime;

@end
