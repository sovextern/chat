
//
//  NSDate+Utils.m
//  CBChat
//
//  Created by Наиль  on 21.07.17.
//  Copyright © 2017 Наиль . All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NSDate+CBUtils.h"

@implementation NSDate(CBUtils)

+ (NSString *) stringDateFromTimeInterval:(NSTimeInterval)interval inFormat:(NSString *)format timeZone:(NSTimeZone *)timeZone
{
    NSDate *date = [NSDate dateWithTimeIntervalSince1970:interval];
    NSDateFormatter *dateFormmater = [[NSDateFormatter alloc] init];
    dateFormmater.dateFormat = format;
    dateFormmater.timeZone = timeZone;
    
    return [dateFormmater stringFromDate:date];
}

- (BOOL) isToday
{
    NSDate *date = [NSDate date];
    NSInteger sumToday = [self componentsSumOfDate:date];
    NSInteger sumSelf = [self componentsSumOfDate:self];
    
    return sumToday == sumSelf;
}

- (NSTimeInterval) timeIntervalSinceBeginDay
{
    NSCalendar *currentCalendar = [NSCalendar currentCalendar];
    NSDateComponents *dateComponents = [currentCalendar components:NSCalendarUnitDay | NSCalendarUnitMonth | NSCalendarUnitYear | NSCalendarUnitHour | NSCalendarUnitMinute | NSCalendarUnitSecond fromDate:self];
    dateComponents.calendar = currentCalendar;
    dateComponents.hour = 0;
    dateComponents.minute = 0;
    dateComponents.second = 0;
    
    return [[dateComponents date] timeIntervalSince1970];
}

#pragma mark - utils methods 

-(NSDate *) toLocalTime
{
    NSTimeZone *tz = [NSTimeZone defaultTimeZone];
    NSInteger seconds = [tz secondsFromGMTForDate: self];
    return [NSDate dateWithTimeInterval: seconds sinceDate: self];
}

-(NSDate *) toGlobalTime
{
    NSTimeZone *tz = [NSTimeZone defaultTimeZone];
    NSInteger seconds = -[tz secondsFromGMTForDate: self];
    return [NSDate dateWithTimeInterval: seconds sinceDate: self];
}

- (NSInteger) componentsSumOfDate:(NSDate *)date
{
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDateComponents *compontents = [calendar components:NSCalendarUnitDay | NSCalendarUnitMonth | NSCalendarUnitYear fromDate:date];
    return compontents.day + compontents.month + compontents.year;
}
@end
